/**
*    Opportunity trigger logic for Before/After insert/Update/delete Events
*
*    Modification Log
*
*    Deepthi        PGI    11/06/2014      Opportunity trigger logic for Before/After Insert/Update/Delete 
**/

public class OpportunityEventHandler{
    public static boolean skipExecution = false; // boolean to exempt recursive trigger logic execution
    
    /*
    * Logic to Associate Opportunity.AccountId with existing Account matching  eCommerceAccountNumber
    */
    public static void updateOpportunity(list<Opportunity> opporLst){
        map<String, list<Opportunity>> opporEcomAcctNoMap = new map<String, list<Opportunity>>();
        list<Opportunity> opporEcomLst = new list<Opportunity>();
        
        /* Storing map<opportunity.EcommerceAccountNumber, list<Opportunities>> */
        for(Opportunity oppor:opporLst){
          if(oppor.ECommerce_Account_Number__c != null){
            opporEcomLst = opporEcomAcctNoMap.containskey(oppor.ECommerce_Account_Number__c)?
                            opporEcomAcctNoMap.get(oppor.ECommerce_Account_Number__c):new list<Opportunity>();
            opporEcomLst.add(oppor);
            opporEcomAcctNoMap.put(oppor.ECommerce_Account_Number__c, opporEcomLst);  
          }
          
        }
        //END of Storing opportunity eCommerceAccountNumber in Map
        
        /* START Retrieving Accounts matching the EcommerceAccountNumber and Associate the matching Opportunity with Account */
        if(opporEcomAcctNoMap != null && opporEcomAcctNoMap.size()>0){
        try{
           for(Account act: [select id, ECommerce_Account_Number__c from account 
                        where ECommerce_Account_Number__c != null and ECommerce_Account_Number__c in :opporEcomAcctNoMap.keyset()]){
               if(opporEcomAcctNoMap.containskey(act.ECommerce_Account_Number__c)){
                  for(Opportunity oppor:opporEcomAcctNoMap.get(act.ECommerce_Account_Number__c)){         
                   oppor.accountid = act.id;
                  }
               }
           }
         }
         catch(Exception e){
             system.debug('OpportunityEventHandler Exception : ' + e.getMessage()); 
         }
        }
        /* END logic for associating opportunity with account matching eCommerceAccountNumber */
        system.debug('OpporLst:'+ opporLst);
    }
    
    /**
    * Logic to delete Opportunity LicenseSets on deleting Opportunities
    **/
    public static void deleteOpporLicenseSets(list<opportunity> opporLst){
        delete [select id from licenseset__c where Opportunity__c in :opporLst];
    }

    /**
    * Logic to delete Opportunity License/Assets on deleting Opportunities
    **/ 
    public static void deleteOpporLicenses(list<opportunity> opporLst){
        delete [select id from Asset where Opportunity__c in :opporLst];
    }
    
    /*
    * Logic to create Contracts for Opportunities updated with Opp_Stage__c = 'Closed Won' and eCommerceConvert = true
    */
    public static void createContracts(map<id, Opportunity> newOpporMap, map<id,Opportunity> oldOpporMap){
        list<opportunity> opporLst = new list<opportunity>();
        /* START getting list of opportunities updated with Opp_Stage__c = 'Closed Won' and eCommerceConvert = true */
         for(Opportunity oppor:newOpporMap.values()){
            Opportunity oldOppor = oldOpporMap.get(oppor.id);
            if(oppor.Opp_Stage__c == 'Closed Won' && oppor.ECommerce_Convert__c && 
                (
                    oppor.Opp_Stage__c <> oldOppor.Opp_Stage__c ||
                    oppor.ECommerce_Convert__c <> oldOppor.ECommerce_Convert__c
                )
                ){
                opporLst.add(oppor);
            }
         }
         /* END - getting opportunity list with 'Closed Won' and eCommerceConvert */
         
         /* START logic to create Contracts for opportunity */
         if(opporLst.size()>0){ 
             
             list<Contracts__c> contractsLst = PGI_Utility.createAccountContracts(opporLst);
             
             if(contractsLst.size() > 0){
                insert contractsLst;
             }        
         }
        /* END logic to create Contracts for opportunity */      
    }
    /* Logic to mirror Opportunity Licenseset to OpportunityLineItem using ProductMapping 
        if opportunity.isClosed (StageName = Closed Won/Lost)
    */
    public static void mirrorOpportunityLineItemLicenseSet(map<id, Opportunity> oldOpporMap, map<id, Opportunity> opporMap){
        set<id> closedOpporIdSet = new set<id>();
        
        system.debug('Opportunity Map :' + opporMap);
        
        // OpportunityLineItemMap to Update
        list<opportunityLineItem> opportunityLineItemUpdateLst = new list<OpportunityLineItem>();
        //Retrieve Opportunity updated with isClosed
        for(Opportunity oppor: opporMap.values()){
            if(((oppor.Opp_stage__c == 'Closed Won' && 
                oldOpporMap.get(oppor.id).opp_Stage__c <> 'Closed Won') || 
                (oppor.Opp_stage__c == 'Closed Lost' && 
                oldOpporMap.get(oppor.id).opp_Stage__c <> 'Closed Lost')) 
                && oppor.ecommerce_convert__c == true){
                closedOpporIdSet.add(oppor.id);
            }
        }
        map<String, licenseset__c> planNameLicensesetMap = new map<String, licenseset__c>();
        // Retrieving licensesets for all Closed Opportunities
        if(closedOpporIdSet <> null && closedOpporIdSet.size()>0){
            for(Licenseset__c licset: [Select zAccount__c, Z_Source__c, Test_State__c,  
                                        Subscription_License_Term_Months__c, Start_Date__c, 
                                        SKU__c, Product__c, Price__c, OwnerId, Order_Number__c, 
                                        Opportunity__c, Number_of_UnAssigned_Users__c, 
                                        Number_of_Licensed_Named_Users__c, Name, Line_Number__c, 
                                        Lead__c, Id, HubID__c, Enterprise_Name__c, End_Date__c, 
                                        Description__c, CompanyID__c
                                       From LicenseSet__c 
                                       where opportunity__c in :closedOpporIdSet]){
                planNameLicensesetMap.put(licset.name, licset);
            }
        }
        
        // Retrieving productMappings for all LicenseSet PlanName
        map<String, Product_Mappings__c> planNameProdMappingsMap = new map<String, Product_Mappings__c>();
        if(planNameLicensesetMap <> null && planNameLicensesetMap.size()>0){
            for(Product_Mappings__c prodMapping: [Select  Source_Product_Name__c, 
                                                         SFDC_Product_Name__c, SFDC_Product_ID__c, 
                                                         SFDC_PB_Entry_ID__c, Name, Id 
                                                        From Product_Mappings__c 
                                                        where 
                                                        Source_Product_Name__c in :planNameLicensesetMap.keyset()]){
                if(planNameLicensesetMap.containskey(prodMapping.Source_Product_Name__c)){
                    Licenseset__c licset = planNameLicensesetMap.get(prodMapping.Source_Product_Name__c);
                    
                    // Assigning opprtunityLineItem with licenseset/productMapping values.
                    OpportunityLineItem opporLineItem = new OpportunityLineItem();
                    //opporLineItem.Product Name    = Product Family=LS.Plan Name;
                    opporLineItem.OpportunityId         = licset.Opportunity__c;
                    //opporLineItem.Billing_Frequency__c  = Label.Default_OpportunityLineItem_Frequency;
                    system.debug('prodMapping:'+prodMapping);
                    system.debug('Source_Product_Name__c:'+prodMapping.Source_Product_Name__c);
                    LicenseSet__c billfreq = [select Enterprise_Name__r.billing_frequency__c from LicenseSet__c where name = :prodMapping.Source_Product_Name__c limit 1];
                    //offerings__c offeringsdata = [select billing_frequency__c from offerings__c where Name = :prodMapping.Source_Product_Name__c limit 1];
                    opporLineItem.Billing_Frequency__c  = billfreq.Enterprise_Name__r.billing_frequency__c;
                    opporLineItem.New_Quantity__c        = licset.Number_of_Licensed_Named_Users__c;
                    opporLineItem.Unit_Type__c          = Label.Default_OpportunityLineItem_UnitType;
                    opporLineItem.New_Sales_Price__c    = licset.Price__c;
                    opporLineItem.Contract_Term__c      = licset.Subscription_License_Term_Months__c;
                    opporLineItem.licenseset__c         = licset.id;
                    opporLineItem.PricebookEntryId      = prodMapping.SFDC_PB_Entry_ID__c;
                    opportunityLineItemUpdateLst.add(opporLineItem);
                }
            }
        }

        // Inserting OpportunityLineItem list
        if(opportunityLineItemUpdateLst <>null && opportunityLineItemUpdateLst.size()>0){
            insert opportunityLineItemUpdateLst;
        }
    }
}