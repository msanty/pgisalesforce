trigger SGLockOpportunityFields on Opportunity (after update) {
    try{
        CONMAN_Contract__c cntrctdata = [select CreatedDate,Contract_Status__c from CONMAN_Contract__c where Opportunity__c = :Trigger.new[0].id limit 1];
        decimal minsdifference = ((datetime.now().getTime())/1000/60) - ((cntrctdata.CreatedDate.getTime())/1000/60);
        if(Trigger.Old[0].z_source__c == 'Salesgateway' && cntrctdata.Contract_Status__c == 'Sent To Customer' && 
           UserInfo.getUserId() != '0051300000BAKahAAH' && minsdifference != 0) //0051300000BAKahAAH -> ecommerce customer
        {
         //Opportunity name
         If(Trigger.Old[0].name != Trigger.New[0].name){
           Trigger.new[0].name.addError('Opportunity Name cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
        } 
    } catch(queryexception qe) {
     // No action required - The aquery in try block returns no records while creating and converting lead first time from SG through SGLeadCreateREST.
    }
}