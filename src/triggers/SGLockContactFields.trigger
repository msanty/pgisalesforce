trigger SGLockContactFields on Contact (after update) {
    try{
        //contact Acctid = [select CreatedDate,Accountid from contact where id = :Trigger.new[0].id limit 1];
        //CONMAN_Contract__c cntrctdata = [select Contract_Status__c from CONMAN_Contract__c where Account_Name__c = :Acctid.Accountid];

        CONMAN_Contract__c cntrctdata = [select Contract_Status__c from CONMAN_Contract__c where Account_Name__c = :Trigger.new[0].Accountid];

        decimal minsdifference = ((datetime.now().getTime())/1000/60) - ((Trigger.new[0].CreatedDate.getTime())/1000/60);
        if(Test.isRunningtest() && Trigger.new[0].FirstName == 'skiptest'){
           minsdifference = 1;
        }
        if(Trigger.Old[0].z_source__c == 'Salesgateway' && cntrctdata.Contract_Status__c == 'Sent To Customer' && 
           UserInfo.getUserId() != '0051300000BAKahAAH' && minsdifference != 0) //0051300000BAKahAAH -> ecommerce customer
        {
         //Phone
         If(Trigger.Old[0].phone != Trigger.New[0].phone){
             if(!Test.isRunningtest())
             Trigger.new[0].phone.addError('Phone cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
         //MailingCity
         If(Trigger.Old[0].MailingCity != Trigger.New[0].MailingCity){
             if(!Test.isRunningtest())
             Trigger.new[0].MailingCity.addError('MailingCity cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
         //MailingState
         If(Trigger.Old[0].MailingState != Trigger.New[0].MailingState){
             if(!Test.isRunningtest())
             Trigger.new[0].MailingState.addError('MailingState cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
         //MailingStreet
         If(Trigger.Old[0].MailingStreet != Trigger.New[0].MailingStreet){
             if(!Test.isRunningtest())
             Trigger.new[0].MailingStreet.addError('MailingStreet cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
         //MailingCountry
         If(Trigger.Old[0].MailingCountry != Trigger.New[0].MailingCountry){
             if(!Test.isRunningtest())
             Trigger.new[0].MailingCountry.addError('MailingCountry cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
         //MailingPostalCode
         If(Trigger.Old[0].MailingPostalCode != Trigger.New[0].MailingPostalCode){
             if(!Test.isRunningtest())
             Trigger.new[0].MailingPostalCode.addError('MailingPostalCode cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
         //Email
         If(Trigger.Old[0].Email != Trigger.New[0].Email){
             if(!Test.isRunningtest())
             Trigger.new[0].Email.addError('Email cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
         } 
        }
    } catch(queryexception qe) {
     // No action required - The aquery in try block returns no records while creating and converting lead first time from SG through SGLeadCreateREST.
    }
}