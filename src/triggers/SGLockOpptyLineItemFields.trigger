trigger SGLockOpptyLineItemFields on OpportunityLineItem (after update) {
    try{
        Opportunitylineitem Opptyid = [select CreatedDate,Opportunityid from Opportunitylineitem where id = :Trigger.new[0].id limit 1];
        CONMAN_Contract__c cntrctdata = [select Contract_Status__c from CONMAN_Contract__c where Opportunity__c = :Opptyid.Opportunityid];
        decimal minsdifference = ((datetime.now().getTime())/1000/60) - ((Opptyid.CreatedDate.getTime())/1000/60);
        system.debug('minsdifference:'+minsdifference);
        if(Trigger.Old[0].z_source__c == 'Salesgateway' && cntrctdata.Contract_Status__c == 'Sent To Customer' && 
           UserInfo.getUserId() != '0051300000BAKahAAH' && minsdifference != 0) //0051300000BAKahAAH -> ecommerce customer
        {
          Trigger.new[0].addError('Products cannot be changed until customer Accepts/Rejects contract in Sales gateway'); 
        } 
    } catch(queryexception qe) {
     // No action required - The aquery in try block returns no records while creating and converting lead first time from SG through SGLeadCreateREST.
    }
}